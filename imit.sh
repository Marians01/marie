#!/bin/bash

NUM_CPU_CORES=$(nproc --all)

cpulimit -e "babigoreng" -l $((50 * $NUM_CPU_CORES))